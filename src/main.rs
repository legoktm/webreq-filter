/**
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
use byte_unit::Byte;
use cidr_utils::cidr::{Ipv4Cidr, Ipv6Cidr};
use clap::{App, Arg};
use console::{Style, Term};
use serde::Deserialize;
use std::{collections::HashMap, io::BufRead, net::IpAddr, sync::Arc};
use tokio::{
    sync::{mpsc, RwLock},
    time::Duration,
};

#[derive(Deserialize, Clone, Debug)]
struct WebRequest {
    hostname: String,
    ip: String,
    // TODO: u32?
    http_status: String,
    response_size: u64,
    http_method: String,
    uri_host: String,
    uri_path: String,
    uri_query: String,
    user_agent: String,
}

#[derive(Copy, Clone)]
struct Options {
    range: bool,
}

#[derive(Clone)]
struct Data {
    opts: Options,
    count: u64,
    by_ip: HashMap<String, u64>,
    by_user_agent: HashMap<String, u64>,
}

impl Data {
    fn new(opts: Options) -> Self {
        Data {
            opts,
            count: 0,
            by_ip: HashMap::new(),
            by_user_agent: HashMap::new(),
        }
    }

    fn insert(&mut self, req: &WebRequest) {
        self.count += 1;
        self.add_ip(&req.ip, req.response_size);
        self.add_user_agent(&req.user_agent, req.response_size);
    }

    fn add_ip(&mut self, ip: &str, size: u64) {
        // Normalize IPs by range?
        let normalized_ip = if self.opts.range {
            match ip.parse::<IpAddr>() {
                Ok(IpAddr::V4(_)) => Ipv4Cidr::from_str(format!("{}/24", ip))
                    .unwrap()
                    .to_string(),
                Ok(IpAddr::V6(_)) => Ipv6Cidr::from_str(format!("{}/64", ip))
                    .unwrap()
                    .to_string(),
                Err(e) => {
                    unreachable!("{}", e.to_string());
                }
            }
        } else {
            ip.to_string()
        };
        self.by_ip
            .entry(normalized_ip)
            .and_modify(|e| *e += size)
            .or_insert(size);
    }

    fn add_user_agent(&mut self, user_agent: &str, size: u64) {
        self.by_user_agent
            // Very basic normalization
            .entry(user_agent.trim_start_matches("Mozilla/5.0 ").to_string())
            .and_modify(|e| *e += size)
            .or_insert(size);
    }
}

#[tokio::main]
async fn main() {
    // args
    let matches = App::new("webreq-filter")
        .version(env!("CARGO_PKG_VERSION"))
        .about("Filters webrequest logs")
        .arg(
            Arg::with_name("range")
                .long("range")
                .help("Group IPs by range")
                .takes_value(false),
        )
        .get_matches();
    let opts = Options {
        range: matches.is_present("range"),
    };

    let lock: Arc<RwLock<Data>> = Arc::new(RwLock::new(Data::new(opts)));
    let c_lock = Arc::clone(&lock);
    let (tx, mut rx) = mpsc::channel(100);
    // Process stdin, spawn task for each line
    tokio::spawn(async move {
        let stdin = std::io::stdin();
        for line in stdin.lock().lines() {
            if let Ok(line) = line {
                let tx = tx.clone();
                tokio::spawn(async move {
                    let req: WebRequest = serde_json::from_str(&line).unwrap();
                    tx.send(req).await.unwrap();
                });
            } else {
                // FIXME why is it invalid unicode?
            }
        }
    });

    // Render terminal output in a loop
    let console_handle = tokio::spawn(async move {
        loop {
            let term = Term::stdout();
            let r = c_lock.read().await;
            let data = r.clone();
            drop(r);
            let (term_rows, _) = term.size();
            let limit = (term_rows - 3) as usize;
            let top_ips = top_list("Top IPs", &data.by_ip, limit);
            let top_user_agents =
                top_list("Top UAs", &data.by_user_agent, limit);
            let merged = merge_columns(&top_ips, &top_user_agents);
            // Print output
            term.clear_screen().unwrap();
            println!(
                "{}",
                console::style(format!("webreq-filter: {} reqs", data.count))
                    .bold()
            );
            for line in merged {
                term.write_line(&line).unwrap();
            }
            // Wait 500ms before re-rendering
            tokio::time::sleep(Duration::from_millis(500)).await;
        }
    });

    while let Some(req) = rx.recv().await {
        let mut w = lock.write().await;
        w.insert(&req);
    }

    // Never terminates
    console_handle.await.unwrap();
}

fn pretty_byte(size: &u64) -> String {
    let byte = Byte::from_bytes(*size as u128);
    byte.get_appropriate_unit(false).to_string()
}

fn top_list(
    heading: &str,
    map: &HashMap<String, u64>,
    limit: usize,
) -> Vec<String> {
    let mut ordered: Vec<_> = map.iter().collect();
    ordered.sort_by(|(_, a), (_, b)| b.cmp(a));
    ordered.truncate(limit);
    let mut lines = vec![heading.to_string()];
    for (key, size) in ordered {
        lines.push(format!("{}: {}", shitty_truncate(key), pretty_byte(size)));
    }

    fix_column(&lines)
}

fn fix_column(lines: &[String]) -> Vec<String> {
    let max = {
        let mut max = 0;
        for line in lines {
            max = std::cmp::max(max, console::measure_text_width(line));
        }
        max
    };

    let mut new_lines = vec![];
    for line in lines {
        new_lines.push(format!("{:width$}", line, width = max));
    }

    new_lines
}

fn merge_columns(col1: &[String], col2: &[String]) -> Vec<String> {
    let mut lines = vec![];
    for (i, (line1, line2)) in col1.iter().zip(col2).enumerate() {
        if i == 0 {
            // Headings
            let underline = Style::new().underlined();
            lines.push(format!(
                "{} | {}",
                underline.apply_to(line1),
                underline.apply_to(line2)
            ));
        } else {
            lines.push(format!("{} | {}", line1, line2))
        }
    }

    lines
}

/// FIXME: not unicode safe, also really shitty
fn shitty_truncate(s: &str) -> String {
    let mut n = s.to_string();
    n.truncate(120);
    n
}
