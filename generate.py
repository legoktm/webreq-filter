#!/usr/bin/env python3
# Script to generate loads of fake entries
import json
from pathlib import Path
import random


def make_line():
    ip = random.randint(0, 9)
    ip2 = random.randint(0, 9)
    ip3 = random.randint(0, 9)
    host = random.randint(0, 9)
    size = random.randint(1000, 9999)
    user_agent = random.randint(0, 26)
    if user_agent == 5:
        user_agent = "curl/5"
    line = {
        "hostname": f"cp305{host}.esams.wmnet",
        "sequence": 13643933834,
        "dt": "2021-04-10T00:12:01Z",
        "time_firstbyte": 0.000207,
        "ip": f"127.{ip}.{ip2}.{ip3}",
        "cache_status": "hit-front",
        "http_status": "200",
        "response_size": size,
        "http_method": "GET",
        "uri_host": "upload.wikimedia.org",
        "uri_path": "/wikipedia/commons/thumb/0/00/Edward_VIII_Portrait_-_1936.jpg/100px-Edward_VIII_Portrait_-_1936.jpg",
        "uri_query": "",
        "content_type": "image/jpeg",
        "referer": "https://en.m.wikipedia.org/wiki/Monarchy_of_Australia",
        "user_agent": str(user_agent) * 5,
        "accept_language": "en",
        "x_analytics": "",
        "range": "-",
        "x_cache": "cp3057 hit, cp3057 hit/657",
        "accept": "image/jpeg, image/gif, image/x-xbitmap, image/png;q=0.8, image/webp;q=0.8, */*;q=0.5",
        "backend": "ATS/8.0.8",
        "tls": "vers=TLSv1.2;keyx=prime256v1;auth=ECDSA;ciph=AES128-GCM-SHA256;prot=h1;sess=reused",
    }
    return json.dumps(line)


lines = ""
for i in range(1_000):
    lines += make_line() + '\n'
Path('data.txt').write_text(lines)
